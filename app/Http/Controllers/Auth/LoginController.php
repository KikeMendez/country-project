<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Socialite;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
    * Redirect the user to the Social authentication page.
    *
    * @return Response
    */
     public function redirectToSocialProvider(Request $request)
    {
        if ($request->social === 'facebook') {
            
            return Socialite::driver($request->social)->redirect();

        } elseif ($request->social === 'google') {

            return Socialite::driver($request->social)->redirect();

        }else{

            return redirect('/');
        }       
    }

    /**
    * Obtain the user information from Facebook.
    *
    * @return Redirect to home page
    */
    public function FacebookProviderCallback()
    {
        $facebook_user = Socialite::driver('facebook')->user();
        $social_type = 'facebook';
        $user = $this->findUserAccountOrCreate($social_type,$facebook_user);
        auth()->login($user);
        
        return redirect($this->redirectTo);
        
    }

    /**
    * Obtain the user information from Google.
    *
    * @return Redirect to home page
    */
    public function GoogleProviderCallback()
    {
        $google_user = Socialite::driver('google')->user();
        $social_type = 'google';
        $user = $this->findUserAccountOrCreate($social_type,$google_user);
        auth()->login($user);
        
        return redirect($this->redirectTo);
        
    }

    /**
    * Try to find a user if can not find then create it.
    *
    * @return Object
    */
    private function findUserAccountOrCreate($social_type,$social_user)
    {
        $currentDateTime = Carbon::now('Europe/London');

        $user = User::firstOrNew(
            ['social_type' => $social_type],
            ['social_user_id' => $social_user->getId()]
        );
        
        if($user->exists) return $user;

        $user->fill([
            'social_type'    => $social_type,
            'social_user_id' => $social_user->getId(),
            'name'           => $social_user->getName(),
            'nickname'       => $social_user->getNickname(),
            'gender'         => $social_user->user['gender'],
            'email'          => $social_user->getEmail(),
            'avatar'         => $social_user->avatar_original,
            'social_token'   => $social_user->token,
            'created_at'     => $currentDateTime,
            'updated_at'     => $currentDateTime
        ])->save();
        // SEND A WELCOME EMAIL TO THE USER
        return $user;
    }

    public function logout()
    {
        auth()->logout();
        return redirect('/');
    }


}

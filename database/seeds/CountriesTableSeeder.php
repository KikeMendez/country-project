<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class CountriesTableSeeder extends Seeder
{
    /**
    * Run the database seeds.
    *
    * @return void
    */
    public function run()
    {
        // Seed Countries Database.
        $currentLondonDateTime = Carbon::now('Europe/London');

        DB::table('countries')->truncate();
        DB::table('countries')->insert([
            'alpha_2'    => 'AF',
            'alpha_3'    => 'AFG',
            'country'   => 'Afghanistan',
            'capital_city'  =>  'Kabul',
            'official_language' =>  'Pashto and Dari',
            'official_currency' =>  'Afghani',
            'demonym'   =>  'Afghan',
            'population'    =>  '34.66 million',
            'dialing_code'  =>  '+93',
            'latitude'  =>  '34.5553',
            'longitude' =>  '69.2075',
            'id_continent'  =>  1,
            'created_at'    =>  $currentLondonDateTime,
            'updated_at'    =>  $currentLondonDateTime
        ]);

        DB::table('countries')->insert([
            'alpha_2'    => 'AL',
            'alpha_3'    => 'ALB',
            'country'   => 'Albania',
            'capital_city'  =>  'Tirana',
            'official_language' =>  'Albanian',
            'official_currency' =>  'Lek',
            'demonym'   =>  'Albanian',
            'population'    =>  '2.876 million',
            'dialing_code'  =>  '+355',
            'latitude'  =>  '41.3275',
            'longitude' =>  '19.8187',
            'id_continent'  => 2,
            'created_at'    =>  $currentLondonDateTime,
            'updated_at'    =>  $currentLondonDateTime
        ]);

        // DB::table('countries')->insert([
        //     'alpha2'    => '',
        //     'alpha3'    => '',
        //     'country'   => '',
        //     'capital_city'  =>  '',
        //     'official_language' =>  '',
        //     'official_currency' =>  '',
        //     'demonym'   =>  '',
        //     'population'    =>  '',
        //     'dialing_code'  =>  '',
        //     'latitude'  =>  '',
        //     'longitude' =>  '',
        //     'id_continent'  => 1,
        //     'created_at'    =>  $currentLondonDateTime,
        //     'updated_at'    =>  $currentLondonDateTime
        // ]);
    }
}

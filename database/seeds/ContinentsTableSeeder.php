<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ContinentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
				$currentLondonDateTime = Carbon::now('Europe/London');
    		DB::table('continents')->truncate();

    		DB::table('continents')->insert([
    			'name'	=>	'Asia',
    			'demonym'	=>	'Asian',
    			'area'	=>	'44.58 million km²',
    			'picture'	=>	'',
    			'population'	=>	'4.436 billion ',
    			'created_at'	=>	$currentLondonDateTime,
    			'updated_at'	=>	$currentLondonDateTime
    		]);

    		DB::table('continents')->insert([
    			'name'	=>	'Europe',
    			'demonym'	=>	'European',
    			'area'	=>	'10.18 million km²',
    			'picture'	=>	'',
    			'population'	=>	'743.1 million ',
    			'created_at'	=>	$currentLondonDateTime,
    			'updated_at'	=>	$currentLondonDateTime
    		]);

    		DB::table('continents')->insert([
    			'name'	=>	'Oceania',
    			'demonym'	=>	'European',
    			'area'	=>	'8.526 million km²',
    			'picture'	=>	'',
    			'population'	=>	'40.5 million',
    			'created_at'	=>	$currentLondonDateTime,
    			'updated_at'	=>	$currentLondonDateTime
    		]);

    		DB::table('continents')->insert([
    			'name'	=>	'South America',
    			'demonym'	=>	'South American',
    			'area'	=>	'17.84 million km²',
    			'picture'	=>	'',
    			'population'	=>	'422.5 million',
    			'created_at'	=>	$currentLondonDateTime,
    			'updated_at'	=>	$currentLondonDateTime
    		]);

    		DB::table('continents')->insert([
    			'name'	=>	'North America',
    			'demonym'	=>	'North American',
    			'area'	=>	'24.71 million km²',
    			'picture'	=>	'',
    			'population'	=>	'579.0 million',
    			'created_at'	=>	$currentLondonDateTime,
    			'updated_at'	=>	$currentLondonDateTime
    		]);

    		DB::table('continents')->insert([
    			'name'	=>	'Africa',
    			'demonym'	=>	'African',
    			'area'	=>	' 30.37 million km²',
    			'picture'	=>	'',
    			'population'	=>	'1.216 billion',
    			'created_at'	=>	$currentLondonDateTime,
    			'updated_at'	=>	$currentLondonDateTime
    		]);

    			DB::table('continents')->insert([
    			'name'	=>	'Antartica',
    			'demonym'	=>	'Antarctican',
    			'area'	=>	' 14 million km²',
    			'picture'	=>	'',
    			'population'	=>	'5.000 temporary residents',
    			'created_at'	=>	$currentLondonDateTime,
    			'updated_at'	=>	$currentLondonDateTime
    		]);

    }
}

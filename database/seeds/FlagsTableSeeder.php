<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class FlagsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
			// Seed Countries Database.
			$currentLondonDateTime = Carbon::now('Europe/London');
			DB::table('flags')->truncate();
			DB::table('flags')->insert([
				'alpha_3'			=>	'AFG',
				'flag_16px'		=>	'afghanistan_16px.png',
				'flag_24px'		=>	'afghanistan_24px.png',
				'flag_32px'		=>	'afghanistan_32px.png',
				'flag_64px'		=>	'afghanistan_64px.png',
				'flag_128px'	=>	'afghanistan_128px.png',
				'flag_256px'	=>	'afghanistan_256px.png',
				'flag_512px'	=>	'afghanistan_512px.png',
				'created_at'  =>  $currentLondonDateTime,
				'updated_at'  =>	$currentLondonDateTime
			]);

			DB::table('flags')->insert([
				'alpha_3'			=>	'ALB',
				'flag_16px'		=>	'albania_16px.png',
				'flag_24px'		=>	'albania_24px.png',
				'flag_32px'		=>	'albania_32px.png',
				'flag_64px'		=>	'albania_64px.png',
				'flag_128px'	=>	'albania_128px.png',
				'flag_256px'	=>	'albania_256px.png',
				'flag_512px'	=>	'albania_512px.png',
				'created_at'  =>  $currentLondonDateTime,
				'updated_at'  =>	$currentLondonDateTime
			]);

			// DB::table('flags')->insert([
			// 	'alpha_3'	=>	'',
			// 	'flag_16px'	=>	'',
			// 	'flag_24px'	=>	'',
			// 	'flag_32px'	=>	'',
			// 	'flag_64px'	=>	'',
			// 	'flag_128px'	=>	'',
			// 	'flag_256px'	=>	'',
			// 	'flag_512px'	=>	'',
			// 	'created_at'    =>  $currentLondonDateTime,
			// 	'updated_at'    =>  $currentLondonDateTime
			// ]);	 
    }
}

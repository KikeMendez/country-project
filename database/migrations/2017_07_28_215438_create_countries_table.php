<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('countries');
        Schema::create('countries', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('alpha_2',2)->unique();
            $table->string('alpha_3',3)->unique();
            $table->string('country',50);
            $table->string('capital_city',50);
            $table->string('official_language',30);
            $table->string('official_currency',30);
            $table->string('demonym',50);
            $table->string('population',30);
            $table->string('dialing_code',10)->unique();
            $table->string('latitude',20)->unique();
            $table->string('longitude',20)->unique();
            $table->integer('id_continent');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}

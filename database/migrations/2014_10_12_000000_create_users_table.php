<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('social_type',20);
            $table->string('social_user_id');
            $table->string('name');
            $table->string('nickname')->nullable();
            $table->string('gender');
            $table->string('email')->unique();
            $table->string('avatar');
            $table->tinyinteger('account_status')->default(0);
            $table->tinyinteger('user_status')->default(0);
            $table->string('social_token');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

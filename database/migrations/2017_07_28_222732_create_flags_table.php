<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFlagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('flags');
        Schema::create('flags', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('alpha_3')->unique();          
            $table->string('flag_16px');
            $table->string('flag_24px');
            $table->string('flag_32px');
            $table->string('flag_64px');
            $table->string('flag_128px');
            $table->string('flag_256px');
            $table->string('flag_512px');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flags');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Logout Route
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

// Redirect To Social Provider
Route::get('login/{social}', 'Auth\LoginController@redirectToSocialProvider');

// Social Login Callback
Route::get('login/facebook/callback', 'Auth\LoginController@FacebookProviderCallback');
Route::get('login/google/callback', 'Auth\LoginController@GoogleProviderCallback');

// Auth::routes();

// Home Route
Route::get('/home', 'HomeController@index')->name('home');

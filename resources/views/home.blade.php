@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    You are logged in!
                    <h1>This is the dashboard {{ Auth::user()->name }}</h1>
                    <h2>My email address is {{ Auth::user()->email }} </h2>
                    <img src="{{ Auth::user()->avatar }}" alt="">

                    <h1>I know what you're thinking... yeah this is so weird</h1>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
